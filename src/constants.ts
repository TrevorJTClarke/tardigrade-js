export const TARDIGRADE_SATELLITES = {
  CENTRAL_1: 'us-central-1.tardigrade.io',
  EUROPE_WEST_1: 'europe-west-1.tardigrade.io',
  ASIA_EAST_1: 'asia-east-1.tardigrade.io'
};
