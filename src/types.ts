/**
 * The options for the Tardigrade class.
 *
 * @interface
 */
export interface TardigradeConfig {
  apiKey: string;
  satellite: Satellite;

  // base options
  includeLogs?: boolean;
  logName?: string;
  logLevel?: string;
}


/**
 * Satellite Options
 */
export enum Satellite {
  CENTRAL_1 = 'us-central-1.tardigrade.io',
  EUROPE_WEST_1 = 'europe-west-1.tardigrade.io',
  ASIA_EAST_1 = 'asia-east-1.tardigrade.io'
}
