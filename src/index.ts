/**
 * The Tardigrade instance class.
 *
 * @module Tardigrade
 */
import pino, { Logger } from 'pino';
import { TardigradeConfig, Satellite } from './types';

/**
 * The base class wrapping all methods for interacting with tardigrade.io
 *
 * @class Tardigrade
 */
class Tardigrade<C extends TardigradeConfig> {
  // Used for config of environment variables
  public config: C;

  protected logger?: Logger;

  /**
   * Create a new Tardigrade instance
   *
   * @param {TardigradeConfig} options
   */
  constructor(options: C) {
    if (!options || !options.apiKey) throw "No API Key configured, please add to options.";


    // initialize base configuration
    this.config = options;
    this.config.satellite = Satellite.CENTRAL_1;

    this._configure(options);

    return this;
  }

  /**
   * Configure the Tardigrade instance
   *
   * @param {TardigradeConfig} options
   * @private
   */
  private _configure(options: C): void {
    // Configure the logger
    if (options.includeLogs)
      this.logger = pino({
        name: options.logName || 'Tardigrade-js',
        level: options.logLevel || 'trace',
      });
  }
}

export default Tardigrade;
