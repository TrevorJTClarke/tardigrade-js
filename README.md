# tardigrade-js

A module to interface with tardigrade.io with a simple api.

Original inspiration by [utropicmedia](https://github.com/utropicmedia/storj-nodejs).

#### Goals

1. Create a simple drop-in replacement for aws-sdk S3 module
2. Methods that are default asynchronous
3. Verbose errors and full coverage handling
4. Full test coverage

## Quick Start
```
$ npm install tardigrade-js
```

Then in your code:
```
import Tardigrade from 'tardigrade-js'

// Create the client instance
const td = new Tardigrade('api_key')

// Create a new bucket
await td.createBucket(params)

// List a buckets items
await td.listObjects(params)

// Upload to a bucket
await td.upload(params)

// Download from a bucket
await td.getObject(params)

// Delete from a bucket
await td.deleteObject(params)
```

### Documentation

TODO: full API list here soon.

### Contributions

Any help is welcome :)
